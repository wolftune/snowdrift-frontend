import "./style.css";
// @ts-ignore
const { Elm } = require("./src/Main.elm");
const pagesInit = require("elm-pages");

pagesInit({
  mainElmModule: Elm.Main
}).then(app => {
  // TODO ports

  app.ports.storeLocally.subscribe(overwriteStoredData)
})



// Let’s store the state of our app in local storage. Unfortunately, Elm doesn’t provide a package for directly accessing local storage. We’ll have to go through JavaScript.
// Note: The Elm development team is working hard to expand support for all technologies included in the Web Platform. Local storage is one of them. In the meantime, the recommended approach is to use JavaScript APIs through ports.

function getStoredData() {
    return localStorage.get('snowdrift');
}

function overwriteStoredData(newdata) {
    localStorage.put("snowdrift", newdata);
}
