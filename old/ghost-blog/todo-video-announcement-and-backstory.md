---
title: "TODO: video announcement and backstory and future thoughts"
slug: todo-video-announcement-and-backstory
date_published: 1970-01-01T00:00:00.000Z
date_updated: 2018-08-16T23:00:03.000Z
draft: true
---

The first section is as good as I could hope for given the brevity.

I hope it's clear that we've presented *examples* of public goods. Paywalls and ads are clear. The visuals emphasize the surveillance that typically accompanies those (tracking ostensibly helps catch people who try to circumvent the paywalls and helps make ads more effective through personalized targetting).

I'd like to emphasize that the road is well-maintained because of the funding paywalls and ads provide. In the snowdrift metaphor, that's hiring snow-plows along with other sorts of road maintenance and construction.

The public road with no surveillance, no tolls, and no ads could be made more clearly beautiful, showing the potential more. It could be clearer that the snow is really troublesome. Maybe add potholes and other issues to emphasize the under-funded condition.

In principle, the same resources that go to maintaining the toll road could instead go to the public road and we'd have the best total package. There's no time in the video to describe the freeriding situation more explicitly. We have to hope people get the gist.

Although the video shows cooperation through more people shoveling snow, it would work as well to have everyone contribute to a fund that hires a dedicated maintenance team. We could succeed at getting the results either way, whether through volunteer labor, voluntary donations, or a mix.

Note that in practice today, public roads get funded through mandatory taxation. That solves the dilemma too. Many of us would be happy if we could just have new taxes funding all sorts of other public goods. But there are lots of valid concerns about that, and anyway we aren't in position to create those sorts of policies. So, we're relying on voluntary donations.

---

The scene introducing crowdmatching and emphasizing monthly sustaining is inadequate and confusing.

For a later edition, I want that scene changed. The two core points are introducing the term "crowdmatching" and a place to mention monthly donations.

---

I'll describe more of my thoughts about the challenges and inadequacies of the current video in a blog post announcing the video. I've never felt good about trying to fit everything in 60 seconds.

Sticking to that short time, I would have personally preferred 60 seconds of just problem-statement and a vague "we're solving this with *crowdmatching!* Learn more…" and then either an illustrated article or a second video to get into the details of how crowdmatching works.

One approach to a single video: problem statement with some deeper meaningful examples or concrete stuff with a hint

Another single video option: just a clear description of how the crowdmatching mechanism works so nobody misunderstands the facts of it (e.g. understand that it's not anything like a Ponzi scheme, it doesn't matter who pledges first — all patrons match each other, all donate the same).

Some of us insisted on each of these, so we have a video that tries to be both and so accomplishes each of these things halfway.

It takes more than 60 seconds to express this stuff, but I think we could do it in 180. From my view, the ideal intro would take more like 3 × 60-second videos, and maybe summaries of the points will help:

1. 
FLO public goods dilemma:

- [Here's concrete examples of public goods].
- Public goods mean non-rivalrous & open to all — i.e. everyone can use the resources limitlessly while freeriding.
- The *only* way *other than taxation* to fund the development and maintenance of public goods is through voluntary donations.
- While existing fundraising does something, it's still orders of magnitude less than the funding from ads and paywalls
- Yet if far more of us cooperated in funding these things, we'd *all* reap the benefits

2. 
Crowdmatching explained:

- Crowdmatching Pledge: Each month, I will donate 0.1¢ for each patron who gives with me, i.e. $1 per 1,000 patrons
- The whole point is for otherwise freeriders to be willing to start donating *if* others join in too — and flexibly, not all-or-nothing.
- **If enough people embrace this and we *really* change the world, maybe we can
