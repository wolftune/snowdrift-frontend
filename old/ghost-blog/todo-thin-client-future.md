---
title: "TODO: thin-client future…"
slug: todo-thin-client-future
date_published: 1970-01-01T00:00:00.000Z
date_updated: 2018-06-15T00:20:47.000Z
draft: true
---

TODO: blog about the FLO issues and public-goods weirdnesses etc. around thin-client future where everything is a web-app or otherwise runs on centralized servers etc. (and how Snowdrift.coop does and doesn't fit into this)
