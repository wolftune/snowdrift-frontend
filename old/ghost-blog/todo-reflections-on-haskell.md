---
title: "TODO: reflections on Haskell"
slug: todo-reflections-on-haskell
date_published: 1970-01-01T00:00:00.000Z
date_updated: 2018-06-19T04:51:38.000Z
draft: true
---

The concepts and principles may be just great, but the reality of how widespread (or not) it is, the build process, tooling, overall the question of whether we invested in an upstream that wasn't or isn't in position to support what we need… this has been a huge pain all along our history, discuss honestly and share perspectives… (should be written primarily by Bryan with input from others as needed)
