---
title: "TODO: blog about middle-men"
slug: todo-blog-about-middle-men
date_published: 1970-01-01T00:00:00.000Z
date_updated: 2018-06-15T00:22:17.000Z
draft: true
---

write about:

the bullshit of just "capturing" value for profit, esp. where middle-men not needed; Snowdrift.coop curates, provides values, and empowers participants in our market; want to expand FLO, not just capture it here.
