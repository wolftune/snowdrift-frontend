---
title: "TODO: blog about CoC 2.0"
slug: todo-blog-about-coc-2-0
date_published: 1970-01-01T00:00:00.000Z
date_updated: 2018-10-20T04:21:34.000Z
draft: true
---

see all the links and notes in [https://community.snowdrift.coop/t/codes-of-conduct-adoption-of-standards/965](https://community.snowdrift.coop/t/codes-of-conduct-adoption-of-standards/965)

Further notes:

# CoC meta discussion / blog post

Unfortunately, many codes use vague mandates (such as "be excellent") and lack adequate processes for stopping harmful behavior.

Some other codes go to the opposite extreme with hardline prohibitions and strict punishments.

Even when codes have nuanced lists of both encouraged and prohibited behavior, the only enforcement they typically offer is reporting violations to authorities.

Because formal reports to authorities are a serious action (which is also costly to those handling the reports), it only gets used for particularly blatant violations.

For any more subtle cases, participants are left to improvise solutions with little or no guidance or simply do nothing (resulting in feelings of frustration and disempowerment resulting in some people disengaging or outright leaving the community).

A full solution involves a holistic system with more empowering tools, norms, and a Code fit to them.

The key concepts which must be honored in all aspects of a holistic system:

    Cut off (hide, collapse, or otherwise stop) harmful posts or behavior as soon as possible to reduce further tensions or harms
    
    Except for egregious repeat offenders or provably malicious actors, address specific posts or behaviors with no other penalties for the person otherwise
    
    Someone alleging a violation may or may not help resolve it
    
    Address all tensions separately from normal discussion itself
    
    Participants must come to consensus on whether to work on resolutions publicly (but away from the original context) or privately, with or without facilitation help, and so on
    
    if someone accused of conduct violation won't consent to any resolution process, their posts or participation privileges simply won't be restored
    
    though everyone is empowered and encouraged to directly address tensions, if someone bringing up a violation doesn't want to participate in resolution, they are still free to simply refer the concern to authorities
    
    With a satisfactory resolution of an issue, everyone remains in good standing and can simply continue normal participation
    

Our system, which works best (but still imperfectly) in our Discourse forum… (summarize why flag-hide-edit-repost along with the ability to do PMs and public meta-category stuff is so important)

[describe somewhere the anti-patterns around typical CoC situations, how people don't report but argue publicly, derail conversations with meta concerns, make things personal, feel rebuked and unwelcome when they get flagged etc]

[emphasize the significance of getting everyone at the BEGINNING to expect that others WILL likely flag them at some point or otherwise bring up tensions, and that in OUR case, they don't need to feel worried or defensive because this is a normal process, not that different from posting in the wrong topic and having someone either move or suggest a move — unconstructive criticism or things that can be taken as condescending etc. are mistakes we all make at times; we want everyone to be *used* to the idea of frequently engaging with CoC and resolution processes, and by having that habit, the whole community will be in the best shape to readily deal with any CoC issues, minor or major — and if we did the frequent thing via frequent reports to authority that would be the anti-pattern of "community policing" where mods are around all the time in mod mode keeping everyone on edge; instead, our point is that what's *frequent* is the self-help approach to everyone helping one another fix mistakes, and the calling on authorities remains rare or even never]

We built the Snowdrift.coop Code of Conduct after thorough research into the best practices from a long list of resources and references.[[1]](#fn1)

    empower participants to resolve issues in ways that help everyone learn and grow
    
    
    setting high expectations to inspire participants to be their best
    

maybe emphasize fuzziness and difficulty, e.g. the names issue and appropriation

(per safe-spaces / safety: there *is* a trade-off between freedom+privacy vs safety+security; the whole idea that the *primary* way to treat marginalized folks is to make them SAFE is a push against freedoms; being free *is* vulnerable)

mention in general: always easier to break something than to fix it

ON AGF:

Some communities call on participants to "Assume Good Faith" (AGF). We had that in an older version of our CoC. The intent is to avoid careless assumptions of bad faith and so on. But mandating AGF may discourage legitimate concerns.

Because we now include *acting* with honesty and good faith in our CoC, any questions of someone violating that pledge can (and must) go through our conduct enforcement procedures — just like any other CoC issue. That way, we still block accusations of dishonesty or bad faith from poisoning normal discussion.

If you feel any doubt at any time about another participant's honesty or good faith, you may either (or both):

(A) assume honesty and good faith anyway while you engage in any further regular discussion

(B) follow our enforcement procedures described below to address the concern

In other words, assumption of good faith is a requirement on all sides in order

to engage in normal discourse. But if there's any reason to doubt the validity

of that assumption, please *do* address the concern (just not within the normal

discussion).

- add a link to this blog post at end of CoC:

- (coming soon) [Snowdrift.coop Code of Conduct background and history] — a blog post describing in depth how and why we developed this Code of Conduct

maybe something about the trans issues, respecting requested pronouns, get into the motivation of it all, not just the act

---

1. 
footnote link to long article etc? or maybe just later flesh out this stuff [↩︎](#fnref1)
