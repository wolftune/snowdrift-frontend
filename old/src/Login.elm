module Login exposing (..)

import Css
import Html as RawHtml
import Html.Styled as Html exposing (..)
import Html.Styled.Attributes as Attr exposing (..)
import Markdown.Html
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)


loginBlockTag =
    Markdown.Html.tag "login"
        (\classes renderedChildren ->
            loginBlock renderedChildren classes
        )
        |> Markdown.Html.withAttribute "class"


loginBlock classes children =
    Html.toUnstyled <|
        Html.form
            [ method "post"
            , enctype "application/x-www-form-urlencoded"
            , action "https://snowdrift.coop/auth/login"
            ]
            [ label [ for "email-input" ] [ text "Email" ]
            , input [ id "email-input", required True ] []
            , label [ for "password-input" ] [ text "Password" ]
            , input [ id "password-input", required True, minlength 9 ] []
            , button [ type_ "submit" ] [ text "Create Account" ]
            , input [ type_ "hidden", name "_token", value "sEy2g4k8DS" ] []
            ]


type alias ViewModel =
    { usernameField : String
    , passwordField : String
    }



-- view
