module Member exposing (Member)


type alias Member =
    { loginEmail : Email
    , name : Name
    }



-- TODO find a nice premade type and validator


type Email
    = Email String


type Name
    = Name
        { first : String
        , full : String
        }
