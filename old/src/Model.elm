module Model exposing (..)

import Member
import Time


type Model
    = NotRunning
    | Running AppModel


type alias AppModel =
    { time : Time.Posix
    , loggedInUser : Maybe Member.Member
    }
