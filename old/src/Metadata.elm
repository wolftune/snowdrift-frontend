module Metadata exposing (ArticleMetadata, Metadata(..), PageMetadata, decoder, ghostDecoder)

import Data.Author
import Date exposing (Date)
import Json.Decode as JD
import List.Extra
import Pages
import Pages.ImagePath as ImagePath exposing (ImagePath)


type Metadata
    = Page PageMetadata
    | Article ArticleMetadata
    | Author Data.Author.Author
    | BlogIndex


type alias ArticleMetadata =
    { title : String
    , description : String
    , published : Date
    , author : Maybe Data.Author.Author
    , image : Maybe (ImagePath Pages.PathKey)
    , draft : Bool
    }


type alias PageMetadata =
    { title : String }


decoder : JD.Decoder Metadata
decoder =
    JD.field "type" JD.string
        |> JD.andThen
            (\pageType ->
                case pageType of
                    "page" ->
                        JD.field "title" JD.string
                            |> JD.map (\title -> Page { title = title })

                    "blog-index" ->
                        JD.succeed BlogIndex

                    "author" ->
                        JD.map3 Data.Author.Author
                            (JD.field "name" JD.string)
                            (JD.field "avatar" imageDecoder)
                            (JD.field "bio" JD.string)
                            |> JD.map Author

                    "blog" ->
                        JD.map6 ArticleMetadata
                            (JD.field "title" JD.string)
                            (JD.field "description" JD.string)
                            (JD.field "published"
                                (JD.string
                                    |> JD.andThen
                                        (\isoString ->
                                            case Date.fromIsoString isoString of
                                                Ok date ->
                                                    JD.succeed date

                                                Err error ->
                                                    JD.fail error
                                        )
                                )
                            )
                            (JD.field "author" (JD.maybe Data.Author.decoder))
                            (JD.field "image" (JD.maybe imageDecoder))
                            (JD.field "draft" JD.bool
                                |> JD.maybe
                                |> JD.map (Maybe.withDefault False)
                            )
                            |> JD.map Article

                    _ ->
                        JD.fail ("Unexpected page type " ++ pageType)
            )


type alias GhostArticleMetadata =
    { title : String
    , slug : String
    , date_published : Date
    , date_updated : Date
    , excerpt : Maybe String
    , draft : Bool
    }


ghostBlogArticleJsonDecoder : JD.Decoder GhostArticleMetadata
ghostBlogArticleJsonDecoder =
    let
        dateDecoder =
            JD.string
                |> JD.andThen
                    (\isoString ->
                        case Date.fromIsoString (String.left 10 isoString) of
                            Ok date ->
                                JD.succeed date

                            Err error ->
                                JD.fail error
                    )
    in
    JD.map6 GhostArticleMetadata
        (JD.field "title" JD.string)
        (JD.field "slug" JD.string)
        (JD.field "date_published" dateDecoder)
        (JD.field "date_updated" dateDecoder)
        (JD.field "excerpt" (JD.maybe JD.string))
        (JD.field "draft" JD.bool
            |> JD.maybe
            |> JD.map (Maybe.withDefault False)
        )


ghostToNormalBlogEntry : GhostArticleMetadata -> Metadata
ghostToNormalBlogEntry ghost =
    Article
        { title = ghost.title
        , description = Maybe.withDefault "" ghost.excerpt

        -- OPINIONATED here we're preferring the more recent date
        , published = ghost.date_updated
        , author = Nothing
        , image = Nothing
        , draft = ghost.draft
        }


ghostDecoder : JD.Decoder Metadata
ghostDecoder =
    JD.map ghostToNormalBlogEntry ghostBlogArticleJsonDecoder


imageDecoder : JD.Decoder (ImagePath Pages.PathKey)
imageDecoder =
    JD.string
        |> JD.andThen
            (\imageAssetPath ->
                case findMatchingImage imageAssetPath of
                    Nothing ->
                        "I couldn't find that. Available images are:\n"
                            :: List.map
                                ((\x -> "\t\"" ++ x ++ "\"") << ImagePath.toString)
                                Pages.allImages
                            |> String.join "\n"
                            |> JD.fail

                    Just imagePath ->
                        JD.succeed imagePath
            )


findMatchingImage : String -> Maybe (ImagePath Pages.PathKey)
findMatchingImage imageAssetPath =
    Pages.allImages
        |> List.Extra.find
            (\image ->
                ImagePath.toString image
                    == imageAssetPath
            )
