module Footer exposing (viewFooter)

import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Pages
import Pages.ImagePath as ImagePath exposing (ImagePath)
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)


viewFooter : Html msg
viewFooter =
    Html.footer
        []
        [ Html.img [ Attr.src (ImagePath.toString Pages.images.wordmarks.snowdrift.withTagline), css [ py_32, m_auto ] ] []
        , Html.section [ Attr.class "footer-links", css [ grid, grid_cols_3, gap_1, text_2xl, text_green_500 ] ]
            [ Html.div [ css [ col_start_1, col_span_1 ] ]
                [ Html.a [ css [ block ], Attr.href "" ] [ Html.text "How It Works" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Blog" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Forum" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Git" ]
                ]
            , Html.div [ css [ col_start_2, col_span_1 ] ]
                [ Html.a [ css [ block ], Attr.href "" ] [ Html.text "About" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Donate" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Our Sponsors" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Privacy Policy" ]
                , Html.a [ css [ block ], Attr.href "" ] [ Html.text "Terms Of Service" ]
                ]
            , Html.div [ css [ col_start_3, col_span_1 ] ]
                [ Html.text "Our free/libre/open licenses: Creative Commons Attribution Share-Alike 4.0 International for content except trademarks, and GNU AGPLv3+ for code." ]
            ]
        ]
