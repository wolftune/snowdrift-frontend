port module Main exposing (main)

import Blog
import Color
import Css
import Css.Global
import Data.Author as Author
import Date
import Footer
import Head
import Head.Seo as Seo
import Html as RawHtml
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Json.Decode
import Layout
import Login
import Markdown.Block
import Markdown.Html
import Markdown.Parser
import Markdown.Renderer exposing (defaultHtmlRenderer)
import Member
import Metadata exposing (Metadata)
import Model exposing (..)
import MySitemap
import Page.Article
import Pages exposing (images, pages)
import Pages.Manifest as Manifest
import Pages.Manifest.Category
import Pages.PagePath exposing (PagePath)
import Pages.Platform
import Pages.StaticHttp as StaticHttp
import Palette
import Renderers.Markdown
import RssFeed
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import Task
import Time
import Yaml.Decode


manifest : Manifest.Config Pages.PathKey
manifest =
    { backgroundColor = Just Color.white
    , categories = [ Pages.Manifest.Category.education ]
    , displayMode = Manifest.Standalone
    , orientation = Manifest.Portrait
    , description = "elm-snowdrift - The future of Snowdrift.coop"
    , iarcRatingId = Nothing
    , name = "snowdrift-elm-website"
    , themeColor = Just Color.white
    , startUrl = pages.index
    , shortName = Just "snowdrift"
    , sourceIcon = images.logos.snowdrift.darkBlueRaster
    , icons = []
    }


type alias Rendered =
    Html Msg



-- the intellij-elm plugin doesn't support type aliases for Programs so we need to use this line
-- main : Platform.Program Pages.Platform.Flags (Pages.Platform.Model Model Msg Metadata Rendered) (Pages.Platform.Msg Msg Metadata Rendered)


main : Pages.Platform.Program Model Msg Metadata Rendered Pages.PathKey
main =
    Pages.Platform.init
        { init = \_ -> init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , documents = [ markdownDocument ]
        , manifest = manifest
        , canonicalSiteUrl = canonicalSiteUrl
        , onPageChange = Nothing
        , internals = Pages.internals
        }
        |> Pages.Platform.withFileGenerator generateFiles
        |> Pages.Platform.toProgram


generateFiles :
    List
        { path : PagePath Pages.PathKey
        , frontmatter : Metadata
        , body : String
        }
    ->
        StaticHttp.Request
            (List
                (Result
                    String
                    { path : List String
                    , content : String
                    }
                )
            )
generateFiles siteMetadata =
    StaticHttp.succeed
        [ RssFeed.fileToGenerate { siteTagline = siteTagline, siteUrl = canonicalSiteUrl } siteMetadata |> Ok
        , MySitemap.build { siteUrl = canonicalSiteUrl } siteMetadata |> Ok
        ]


markdownDocument : { extension : String, metadata : Json.Decode.Decoder Metadata, body : String -> Result error (Html msg) }
markdownDocument =
    let
        bodyTry inputMarkdownString =
            blockList inputMarkdownString
                |> renderBlocksToHtml
                |> wrapDocument
                |> Ok

        blockList inputMarkdownString =
            Markdown.Parser.parse inputMarkdownString
                |> Result.withDefault []

        renderBlocksToHtml : List Markdown.Block.Block -> List (Html msg)
        renderBlocksToHtml markdownBlocks =
            Result.withDefault [ Html.text "Empty document" ] (Markdown.Renderer.render Renderers.Markdown.snowdriftMarkdownToHtmlRenderer markdownBlocks)

        wrapDocument docHtmlElements =
            Html.section [ Attr.id "markdown-document-contents", Attr.css [ w_full, max_w_3xl, m_auto ] ]
                (docHtmlElements ++ [ Footer.viewFooter ])

        -- TODO do less un/restyling
    in
    { extension = "md"
    , metadata = Json.Decode.oneOf [ Metadata.decoder, Metadata.ghostDecoder ]
    , body = bodyTry
    }


renderer =
    { defaultHtmlRenderer
        | html =
            Markdown.Html.oneOf
                [ Login.loginBlockTag
                ]
    }


init : ( Model, Cmd Msg )
init =
    let
        initAppModel time =
            Running
                { time = time
                , loggedInUser = Nothing
                }
    in
    ( NotRunning, Task.perform StartApp Time.now )


type Msg
    = StartApp Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StartApp time ->
            ( Running
                { time = time
                , loggedInUser = Nothing
                }
            , storeLocally "Hi"
            )



--subscriptions : Model -> Sub Msg


subscriptions : a -> b -> c -> Sub msg
subscriptions _ _ _ =
    Sub.none


view :
    List ( PagePath Pages.PathKey, Metadata )
    ->
        { path : PagePath Pages.PathKey
        , frontmatter : Metadata
        }
    ->
        StaticHttp.Request
            { view : Model -> Rendered -> { title : String, body : RawHtml.Html Msg }
            , head : List (Head.Tag Pages.PathKey)
            }
view siteMetadata page =
    StaticHttp.succeed
        { view =
            \model viewForPage ->
                Layout.view (pageView model siteMetadata page viewForPage) page model
        , head = head page.frontmatter
        }


pageView :
    Model
    -> List ( PagePath Pages.PathKey, Metadata )
    -> { path : PagePath Pages.PathKey, frontmatter : Metadata }
    -> Rendered
    -> { title : String, body : List (Html Msg) }
pageView model siteMetadata page viewForPage =
    case page.frontmatter of
        Metadata.Page metadata ->
            { title = metadata.title
            , body =
                [ Html.article [ Attr.class "page" ] [ viewForPage ] ]
            }

        Metadata.Article metadata ->
            Page.Article.view metadata viewForPage

        Metadata.Author author ->
            { title = author.name
            , body = []

            -- [ Palette.blogHeading author.name
            -- , Author.view [] author
            -- , Html.p [ Attr.css [ object_center ]  ] [ viewForPage ]
            -- ]
            }

        Metadata.BlogIndex ->
            { title = "Snowdrift Blog"
            , body =
                [ Html.section [ Attr.css [ p_20 ] ]
                    -- [ Blog.view siteMetadata ]
                    []
                ]
            }


commonHeadTags : List (Head.Tag Pages.PathKey)
commonHeadTags =
    [ Head.rssLink "/blog/feed.xml"
    , Head.sitemapLink "/sitemap.xml"
    ]



{- Read more about the metadata specs:

   <https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/abouts-cards>
   <https://htmlhead.dev>
   <https://html.spec.whatwg.org/multipage/semantics.html#standard-metadata-names>
   <https://ogp.me/>
-}


head : Metadata -> List (Head.Tag Pages.PathKey)
head metadata =
    commonHeadTags
        ++ (case metadata of
                Metadata.Page meta ->
                    Seo.summaryLarge
                        { canonicalUrlOverride = Nothing
                        , siteName = "Snowdrift"
                        , image =
                            { url = images.logos.snowdrift.darkBlueRaster -- TODO does svg work?
                            , alt = "Snowdrift logo"
                            , dimensions = Nothing
                            , mimeType = Nothing
                            }
                        , description = siteTagline
                        , locale = Nothing
                        , title = meta.title
                        }
                        |> Seo.website

                Metadata.Article meta ->
                    Seo.summaryLarge
                        { canonicalUrlOverride = Nothing
                        , siteName = "snowdrift"
                        , image =
                            { url = Maybe.withDefault Pages.images.logos.snowdrift.darkBlue meta.image
                            , alt = meta.description
                            , dimensions = Nothing
                            , mimeType = Nothing
                            }
                        , description = meta.description
                        , locale = Nothing
                        , title = meta.title
                        }
                        |> Seo.article
                            { tags = []
                            , section = Nothing
                            , publishedTime = Just (Date.toIsoString meta.published)
                            , modifiedTime = Nothing
                            , expirationTime = Nothing
                            }

                Metadata.Author meta ->
                    let
                        ( firstName, lastName ) =
                            case meta.name |> String.split " " of
                                [ first, last ] ->
                                    ( first, last )

                                [ first, middle, last ] ->
                                    ( first ++ " " ++ middle, last )

                                [] ->
                                    ( "", "" )

                                _ ->
                                    ( meta.name, "" )
                    in
                    Seo.summary
                        { canonicalUrlOverride = Nothing
                        , siteName = "snowdrift"
                        , image =
                            { url = meta.avatar
                            , alt = meta.name ++ "'s blog articles."
                            , dimensions = Nothing
                            , mimeType = Nothing
                            }
                        , description = meta.bio
                        , locale = Nothing
                        , title = meta.name ++ "'s blog articles."
                        }
                        |> Seo.profile
                            { firstName = firstName
                            , lastName = lastName
                            , username = Nothing
                            }

                Metadata.BlogIndex ->
                    Seo.summaryLarge
                        { canonicalUrlOverride = Nothing
                        , siteName = "Snowdrift"
                        , image =
                            { url = images.logos.snowdrift.darkBlueRaster
                            , alt = "Snowdrift logo"
                            , dimensions = Nothing
                            , mimeType = Nothing
                            }
                        , description = siteTagline
                        , locale = Nothing
                        , title = "Snowdrift blog"
                        }
                        |> Seo.website
           )


canonicalSiteUrl : String
canonicalSiteUrl =
    "https://snowdrift.gitlab.io/elm-website/"


siteTagline : String
siteTagline =
    "Future Snowdrift.coop"


port storeLocally : String -> Cmd msg
