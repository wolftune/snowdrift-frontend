module Renderers.Markdown exposing (snowdriftMarkdownToHtmlRenderer)

import Css as RawCss exposing (rem)
import Html as RawHtml
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Markdown.Block as Block exposing (Block)
import Markdown.Html
import Markdown.Renderer
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)


snowdriftMarkdownToHtmlRenderer : Markdown.Renderer.Renderer (Html msg)
snowdriftMarkdownToHtmlRenderer =
    { heading =
        \{ level, children } ->
            case level of
                Block.H1 ->
                    Html.h1 [ css [ text_4xl, font_bold, my_8 ] ] children

                Block.H2 ->
                    Html.h2 [ css [ text_3xl, font_bold, my_8 ] ] children

                Block.H3 ->
                    Html.h3 [ css [ text_3xl, my_8 ] ] children

                Block.H4 ->
                    Html.h4 [ css [ text_2xl, font_bold, my_8 ] ] children

                Block.H5 ->
                    Html.h5 [ css [ text_2xl, my_8 ] ] children

                Block.H6 ->
                    bigGreenButton children
    , paragraph = Html.p [ css [ text_2xl, font_light ] ]
    , hardLineBreak = Html.br [] []
    , blockQuote = Html.blockquote []
    , strong =
        \children -> Html.strong [] children
    , emphasis =
        \children -> Html.em [] children
    , strikethrough =
        \children -> Html.del [] children
    , codeSpan =
        \content -> Html.code [] [ Html.text content ]
    , link =
        \link content ->
            case link.title of
                Just title ->
                    Html.a
                        [ Attr.href link.destination
                        , Attr.title title
                        ]
                        content

                Nothing ->
                    Html.a [ Attr.href link.destination ] content
    , image =
        \imageInfo ->
            case imageInfo.title of
                Just title ->
                    Html.img
                        [ Attr.src imageInfo.src
                        , Attr.alt imageInfo.alt
                        , Attr.title title
                        ]
                        []

                Nothing ->
                    Html.img
                        [ Attr.src imageInfo.src
                        , Attr.alt imageInfo.alt
                        ]
                        []
    , text =
        Html.text
    , unorderedList =
        \items ->
            Html.ul []
                (items
                    |> List.map
                        (\item ->
                            case item of
                                Block.ListItem task children ->
                                    let
                                        checkbox =
                                            case task of
                                                Block.NoTask ->
                                                    Html.text ""

                                                Block.IncompleteTask ->
                                                    Html.input
                                                        [ Attr.disabled True
                                                        , Attr.checked False
                                                        , Attr.type_ "checkbox"
                                                        ]
                                                        []

                                                Block.CompletedTask ->
                                                    Html.input
                                                        [ Attr.disabled True
                                                        , Attr.checked True
                                                        , Attr.type_ "checkbox"
                                                        ]
                                                        []
                                    in
                                    Html.li [] (checkbox :: children)
                        )
                )
    , orderedList =
        \startingIndex items ->
            Html.ol
                (case startingIndex of
                    1 ->
                        [ Attr.start startingIndex ]

                    _ ->
                        []
                )
                (items
                    |> List.map
                        (\itemBlocks ->
                            Html.li []
                                itemBlocks
                        )
                )
    , html = Markdown.Html.oneOf []
    , codeBlock =
        \{ body, language } ->
            let
                classes =
                    -- Only the first word is used in the class
                    case Maybe.map String.words language of
                        Just (actualLanguage :: _) ->
                            [ Attr.class <| "language-" ++ actualLanguage ]

                        _ ->
                            []
            in
            Html.pre []
                [ Html.code classes
                    [ Html.text body
                    ]
                ]
    , thematicBreak = Html.hr [] []
    , table = Html.table []
    , tableHeader = Html.thead []
    , tableBody = Html.tbody []
    , tableRow = Html.tr []
    , tableHeaderCell =
        \maybeAlignment ->
            let
                attrs =
                    maybeAlignment
                        |> Maybe.map
                            (\alignment ->
                                case alignment of
                                    Block.AlignLeft ->
                                        "left"

                                    Block.AlignCenter ->
                                        "center"

                                    Block.AlignRight ->
                                        "right"
                            )
                        |> Maybe.map Attr.align
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
            in
            Html.th attrs
    , tableCell =
        \maybeAlignment ->
            let
                attrs =
                    maybeAlignment
                        |> Maybe.map
                            (\alignment ->
                                case alignment of
                                    Block.AlignLeft ->
                                        "left"

                                    Block.AlignCenter ->
                                        "center"

                                    Block.AlignRight ->
                                        "right"
                            )
                        |> Maybe.map Attr.align
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
            in
            Html.td attrs
    }


bigGreenButton : List (Html msg) -> Html msg
bigGreenButton children =
    let
        specialRoundedCorners =
            -- Is there a way to consolidate this?
            [ RawCss.borderBottomLeftRadius2 roundedValue1 roundedValue2
            , RawCss.borderTopLeftRadius2 roundedValue1 roundedValue2
            , RawCss.borderTopRightRadius2 roundedValue1 roundedValue2
            , RawCss.borderBottomRightRadius2 roundedValue1 roundedValue2
            ]

        ( roundedValue1, roundedValue2 ) =
            ( rem 0.6, rem 3 )
    in
    Html.node "big-green-button-section"
        [ css
            [ block
            , text_center -- center the big green button without width
            , my_8
            ]
        ]
        -- use a wrapper to allow centering without width
        [ Html.node "big-green-button"
            [ css
                ([ bg_green_500
                 , inline_block
                 , border_2
                 , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
                 , text_3xl
                 , text_white
                 , font_extrabold
                 , py_3
                 , px_28 -- if text is long enough to not hit min width
                 , min_w_max -- TODO make it a percent?
                 ]
                    ++ specialRoundedCorners
                 -- TODO tailwind approach?
                )
            ]
            children
        ]
