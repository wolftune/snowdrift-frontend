module Layout exposing (view)

import Css as RawCss exposing (important, rem)
import Css.Global
import Html as RawHtml
import Html.Styled as Html exposing (..)
import Html.Styled.Attributes as Attr exposing (..)
import LogoSvg
import Metadata exposing (Metadata)
import Model exposing (..)
import Pages
import Pages.Directory as Directory exposing (Directory)
import Pages.ImagePath as ImagePath
import Pages.PagePath as PagePath exposing (PagePath)
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)


view :
    { title : String, body : List (Html msg) }
    ->
        { path : PagePath Pages.PathKey
        , frontmatter : Metadata
        }
    -> Model
    -> { title : String, body : RawHtml.Html msg }
view document page model =
    { title = document.title
    , body =
        toUnstyled <|
            div
                [ css
                    [ w_full
                    , bg_white
                    , bg_no_repeat
                    , RawCss.backgroundSize2 (RawCss.pct 100) (RawCss.rem 40)
                    , RawCss.backgroundImage <| RawCss.linearGradient2 (RawCss.deg 178) (RawCss.stop2 (RawCss.rgba 202 243 252 1) (RawCss.pct 0)) (RawCss.stop2 (RawCss.rgba 255 255 255 0) (RawCss.pct 50)) []
                    , min_h_screen -- prevent background from being cut off on short pages
                    ]
                ]
                [ globalStyles model
                , header page.path
                , main_
                    [ css
                        [ w_full
                        , py_16 -- move main content down from touching header
                        , grid_cols_12
                        , object_center
                        ]

                    -- main content aria role?
                    ]
                    document.body

                --, loginDialog
                ]
    }


globalStyles model =
    let
        noScriptStyles =
            if model == NotRunning then
                [ Css.Global.class "js-only" [ RawCss.display RawCss.none ] ]

            else
                []
    in
    Css.Global.global
        (Tw.globalStyles
            ++ [ Css.Global.html snowdriftFonts ]
            ++ noScriptStyles
        )


snowdriftFonts =
    [ RawCss.fontFamilies [ "Nunito" ], RawCss.color (RawCss.hex "#13628e") ]


header : PagePath Pages.PathKey -> Html msg
header currentPath =
    Html.header [ css [ w_full, h_16, py_2, px_2, sticky, top_0, bg_white, bg_opacity_50, shadow_lg ] ]
        [ nav
            [ css
                [ m_auto -- so nav is centered in header
                , flex
                , space_x_8
                , max_w_3xl
                , h_full
                , text_green_500
                , font_extrabold
                , text_2xl
                , items_center -- nav items vertically centered
                , justify_between
                ]
            ]
            [ mainLogo --add logo here
            , renderNavLink <| NavLink (PagePath.toString Pages.pages.about.howItWorks) "How it works" False
            , highlightableLink currentPath Pages.pages.projects.directory "Projects"
            , highlightableLink currentPath Pages.pages.about.directory "About"
            , highlightableLink currentPath Pages.pages.blog.directory "Blog"
            , loginLink
            ]
        ]


mainLogo : Html msg
mainLogo =
    let
        logoImagePath : ImagePath.ImagePath Pages.PathKey
        logoImagePath =
            Pages.images.logos.snowdrift.darkBlue
    in
    a
        [ css [ h_full, inline_block, flex_none ]
        , id "main-logo"
        , href (PagePath.toString Pages.pages.index)
        , alt "Snowdrift"
        ]
        [ img
            [ src (ImagePath.toString logoImagePath)
            , css [ h_full, inline_block ]
            ]
            []
        ]


highlightableLink :
    PagePath Pages.PathKey
    -> Directory Pages.PathKey Directory.WithIndex
    -> String
    -> Html msg
highlightableLink currentPath linkDirectory displayName =
    let
        isHighlighted =
            currentPath |> Directory.includes linkDirectory
    in
    a
        [ css
            (if isHighlighted then
                [ underline, text_blue_600, flex_initial ]

             else
                []
            )
        , href (linkDirectory |> Directory.indexPath |> PagePath.toString)
        ]
        [ text displayName ]


type alias NavLink =
    { url : String
    , label : String
    , newTabOpen : Bool
    }



-- gitlabRepoLink : Html msg
-- gitlabRepoLink =
--     a []
--         { url = "https://gitlab.com/snowdrift/elm-website/"
--         , label =
--             Element.image
--                 [ Element.width (Element.px 22)
--                 , Font.color Palette.color.primary
--                 ]
--                 { src = ImagePath.toString Pages.images.logos.gitlab.blackOutline, description = "Gitlab repo" }
--         }
-- elmDocsLink : Html msg
-- elmDocsLink =
--     Element.newTabLink []
--         { url = "https://package.elm-lang.org/packages/dillonkearns/elm-pages/latest/"
--         , label =
--             Element.image
--                 [ Element.width (Element.px 22)
--                 , Font.color Palette.color.primary
--                 ]
--                 { src = ImagePath.toString Pages.images.elmLogo, description = "Elm Package Docs" }
--         }


loginLink : Html msg
loginLink =
    renderNavLink
        { url = PagePath.toString Pages.pages.login
        , label = "Login"
        , newTabOpen = False
        }


renderNavLink : NavLink -> Html msg
renderNavLink navLink =
    a
        [ href navLink.url -- TODO new tab open
        ]
        [ text navLink.label ]


loginDialog : Html msg
loginDialog =
    let
        cardStyle =
            [ bg_white
            , p_6
            , border_gray_200
            , rounded_md
            , border_2
            , grid
            , grid_cols_1
            , space_y_6
            ]

        inputStyle =
            List.map important
                -- !important, since the css sanitize uses input[type=specific] selectors which win the specificity war over our classes
                [ border_gray_100
                , border_2
                , p_2
                , rounded_sm
                , placeholder_gray_400
                , text_2xl
                , font_light
                , text_blue_800 -- TODO use snowdrift blue
                ]
    in
    section
        [ css
            [ fixed
            , top_0 -- TODO is there a combo class?
            , bottom_0
            , right_0
            , left_0
            , bg_blue_100
            , bg_opacity_100
            , grid
            , place_items_center
            ]
        ]
        [ node "dialog"
            [ css
                [ absolute
                , grid
                , space_y_8
                , bg_transparent -- override browser default "dialog" styles
                ]
            ]
            [ img
                [ src (ImagePath.toString Pages.images.logos.snowdrift.darkBlue)
                , css [ m_auto, w_24 ]
                ]
                []
            , h1
                [ css
                    [ text_center
                    , text_3xl
                    , font_thin
                    ]
                ]
                [ text "Log in to Snowdrift.coop" ]
            , div [ css cardStyle ]
                [ --label [ for "email" ] [ text "email" ]
                  input
                    [ id "email-input"
                    , type_ "email"
                    , placeholder "email"
                    , required True
                    , css inputStyle
                    ]
                    []

                -- , label [ for "passphrase" ] [ text "passphrase" ]
                , input
                    [ id "password-input"
                    , placeholder "passphrase"
                    , type_ "password"
                    , required True
                    , minlength 9
                    , css inputStyle
                    ]
                    []
                , button
                    [ type_ "submit"
                    , css
                        ([ bg_green_500
                         , inline_block
                         , border_2
                         , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
                         , text_3xl
                         , text_white
                         , font_extrabold
                         , py_3
                         , px_28 -- if text is long enough to not hit min width]
                         ]
                            ++ specialRoundedCorners
                        )
                    , class "js-only"
                    ]
                    [ text "Log in" ]
                ]
            , div [ css cardStyle ]
                [ text "Don't have an account? Sign Up!" ]
            ]
        ]


specialRoundedCorners =
    let
        ( roundedValue1, roundedValue2 ) =
            ( rem 0.6, rem 3 )
    in
    -- TODO globalize this
    [ RawCss.borderBottomLeftRadius2 roundedValue1 roundedValue2
    , RawCss.borderTopLeftRadius2 roundedValue1 roundedValue2
    , RawCss.borderTopRightRadius2 roundedValue1 roundedValue2
    , RawCss.borderBottomRightRadius2 roundedValue1 roundedValue2
    ]
