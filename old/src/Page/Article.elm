module Page.Article exposing (view)

import Data.Author as Author
import Date exposing (Date)
import Element exposing (Element)
import Element.Font as Font
import Html as RawHtml
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Metadata exposing (ArticleMetadata)
import Pages
import Pages.ImagePath as ImagePath exposing (ImagePath)
import Palette
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)


view : ArticleMetadata -> Html msg -> { title : String, body : List (Html msg) }
view metadata viewForPage =
    let
        authorSection author =
            Element.column [ Element.spacing 10 ]
                [ Element.row [ Element.spacing 10 ]
                    [ Author.view [] author
                    , Element.column [ Element.spacing 10, Element.width Element.fill ]
                        [ Element.paragraph [ Font.bold, Font.size 24 ]
                            [ Element.text author.name
                            ]
                        , Element.paragraph [ Font.size 16 ]
                            [ Element.text author.bio ]
                        ]
                    ]
                ]
    in
    { title = metadata.title
    , body =
        List.singleton <|
            Html.article
                [ Attr.attribute "article-title" metadata.title
                , Attr.class "markdown-article"
                , css []
                ]
                [ --Maybe.withDefault (Html.text "") (Maybe.map authorSection metadata.author)
                  --, publishedDateView metadata |> Element.el [ Font.size 16, Font.color (Element.rgba255 0 0 0 0.6) ]
                  bigTitle metadata.title
                , Maybe.withDefault (Html.text "") (Maybe.map articleImageView metadata.image)
                , viewForPage
                ]
    }


publishedDateView : { a | published : Date } -> Element msg
publishedDateView metadata =
    Element.text
        (metadata.published
            |> Date.format "MMMM ddd, yyyy"
        )


articleImageView : ImagePath Pages.PathKey -> Html msg
articleImageView articleImage =
    Html.img [ css [ w_full ], Attr.src (ImagePath.toString articleImage), Attr.alt "Article cover photo" ] []


bigTitle : String -> Html msg
bigTitle articleTitle =
    Html.h1 [ Attr.class "article-title", css [ text_blue_500, block, w_full, text_center, text_5xl ] ]
        [ Html.text articleTitle ]
