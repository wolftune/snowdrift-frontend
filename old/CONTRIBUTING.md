
# code format
For perfectly clean git diffs, all pushed code must be run through `elm-format`.
The tool has no configuration, so it pretty much eliminates traditional formatting debates.
I suggest you have it run on every save. On Atom this can be achieved with the elm-format plugin.
