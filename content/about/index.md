---
title: About Snowdrift
type: page
---

# About Snowdrift

Snowdrift.coop is a nonprofit cooperative run by an international team driven by a common goal:

> To dramatically improve the ability of ordinary people to fund public goods – things like software, music, journalism, and research – that everyone can use and share without limitations.

## Status

- Functioning
Initial pledging to the platform itself as the first test project
- Next steps
Run a first real charge for active patrons; clean up site design
- Next major milestone
Enabling the first outside projects for active listing

See our roadmap

Our innovative crowdmatching approach is outlined in our overview video and How It Works page. More detail and updates are available in the following places:

## Wiki
Read about the entire background of the project and find various resources in our wiki.

## Community
Browse our forum categories to read, comment or post about anything related to Snowdrift.coop.

## Repo
Squash bugs or have a look at how things are evolving in our git repository.

## Blog
Read occasional updates and thoughts about Snowdrift.coop.
