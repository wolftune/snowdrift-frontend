---
title: How Snowdrift Works
type: page
---

# How it works

Snowdrift.coop is a nonprofit cooperative run by an international team driven by a common goal:

> To dramatically improve the ability of ordinary people to fund public goods – things like software, music, journalism, and research – that everyone can use and share without limitations.
