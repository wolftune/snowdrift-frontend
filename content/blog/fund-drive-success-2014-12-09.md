---
title: Fund-drive already a success
slug: fund-drive-success-2014-12-09
date_published: 2014-12-09T22:02:00.000Z
date_updated: 2017-11-17T05:22:18.000Z
excerpt: Less than two weeks after starting, our launch fund-drive has now *tilted*. We passed the minimum goal of $3,000 for legal costs. We now push ahead toward our stretch goals.
---

**Less than two weeks after starting, our launch fund-drive has now *tilted*.** We passed the minimum goal of $3,000 for legal costs. We now push ahead toward our stretch goals. Further donations will help us finish the necessary functions for the website. Please continue to help us by donating and spreading the word to others.

We have also received substantial donations via Paypal and Dwolla which do not directly count in the totals shown at snowdrift.tilt.com (and we'll provide details on those later). At this point, we have well over 100 patrons to the campaign, and we've now surpassed 500 user accounts on the site, even though we're still only testing.

---

We didn't plan lots of promotion on "social media." Of course, most social platforms work in direct opposition to the values that motivate us at Snowdrift.coop: freedom, openness, democracy, and respect for privacy. Besides sticking to principles, those of us on the Snowdrift.coop team are simply not big users of proprietary social media systems. Of course, Tilt Open includes links to Twitter and Facebook whether we like it or not. To avoid those links, we would have had to self-host (see our previous blog on [choosing Tilt Open](https://blog.snowdrift.coop/2014-11-27-fund-drive-launch#why-tilt-open)).

As it turned out, we did get a decent amount of Twitter attention and had to figure out how to best manage it. We decided to do like the [FSF](https://fsf.org): post to GNU Social and then mirror to Twitter. So, **you can now follow us [@SnowdriftCoop on quitter.se](https://quitter.se/snowdriftcoop)** (and also @SnowdriftCoop on Twitter). We're still not on Facebook and have no plans for it, although people might be independently posting links there. Some other attention has come from Reddit. If there is other chatter about us elsewhere, we may not know about it. In other news, **our updated [press page](https://wiki.snowdrift.coop/press) now links to a handful of excellent independent articles about us.**

Going forward, we hope to finally have e-mail integration with the site soon. That will allow us to more easily keep everyone updated about activity and news. We have several newly interested volunteers, and we'll see how quickly they will make a difference in basic functions of the site and clarity for the pledging process. In the midst of juggling many other things, I hope to find time to make more videos better explaining all sorts of things about our pledge system and more. Stay tuned!
