---
title: Hello Snowdrift.coop Community, I am your "new" Director
slug: new-community-director
date_published: 2016-04-23T00:36:00.000Z
date_updated: 2017-11-10T21:41:33.000Z
excerpt: Hi, my name is William Hale, but feel free to call me Salt. At the beginning of this year, I accepted the role of Community Director for Snowdrift.coop. I am a FLOSS advocate who has gone all-in to make Snowdrift.coop a reality.
---

Hi, my name is William Hale, but feel free to call me Salt. At the beginning of this year, I accepted the role of Community Director for Snowdrift.coop. After spending the last five months traveling abroad, I have finally found time to write this post explaining what the title means and what my future goals are.

---

tl;dr I am a FLOSS advocate who has gone all-in to make Snowdrift.coop a reality.

## Personal background

My interest in Free/Libre/Open software and societies began around 1996 when I became involved with GNU/Linux. From 2007 through 2015, I was Lead Organizer for the Greater Seattle Linux Users Group; I attended the University of Washington and hold a degree in Computer Science and Communications; and I have spent the past few years speaking at a variety of conferences, recently representing Snowdrift.coop at [FOSDEM 2016](https://fosdem.org/2016/schedule/event/snowdriftcoop_sustainable_funding/).

## Joining Snowdrift.coop

When I met Aaron at Snowdrift.coop's booth during LinuxFest Northwest 2015, my interest was caught; at the Seattle GNU/Linux conference later that year, it was held. However, I would need a title and focus if I were to dive in and become part of the core team. Hours of internal debate later, we settled on Community Director.

This position attracted me due to my previous experience and ability to bridge technical and non-technical groups. Also, I have spent the last year researching free economies and other FLOSS topics. This has only increased my desire to support a better world through public goods.

## The role

To me, "Community Director" encompasses a number of things.  Whenever people are meeting, projects are being recruited, or the community is being nurtured, it is my duty to provide clear direction. While this also implies leadership, I am actively seeking volunteers interested in becoming Community Leaders around the world. Of course, I intend to manage all of the local communities which have not yet found leadership.

One of my core responsibilities is to handle all of our contact lists, announcements, and volunteer coordination. Currently, I am setting up CiviCRM, a platform that was designed with non-profits in mind and can be customized to fit our needs well. After moving the volunteer registration form and announce mailing list, I will be building a usability testing group, new local meetup groups for different geographic regions, and more.

## Looking forward

These next few months look to be exciting as we approach our alpha launch of the mechanism. In anticipation of an initial functional release, I decided to take a hiatus from my circumnavigation travel plans to stay in my hometown of Seattle, WA and work full-time on Snowdrift.coop. This way, I can do the most to help with immediate work (especially taking over some responsibilities from co-founder Aaron Wolf during his paternity leave). Once we are launched, I can return to my travels, and promote a working Snowdrift.coop system to those I meet around the world.

I want to thank everyone reading this for your interest in our project. Please contact me with *any* questions, comments, or concerns. I am the go-to contact now for all the people out there interested in being a part of this community, whether on the sidelines or in more active participation. Come say "hi" at our next in-person conference: [LinuxFest Northwest 2016](https://www.linuxfestnorthwest.org/2016) in Bellingham, WA this weekend!

--

William Hale

aka Salt

Community Director

Snowdrift.coop

"Free the Commons"
