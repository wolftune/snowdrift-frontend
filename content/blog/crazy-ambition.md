---
title: Humility, Ambition, Optimism, and Delusion
slug: crazy-ambition
date_published: 2017-11-17T20:31:14.000Z
date_updated: 2017-11-18T07:16:13.000Z
excerpt: We have all the challenges of any idealistic FLO project plus creating a new type of product (not just a FLO version of existing concepts) plus the challenges of building a co-op/movement/community plus delving into financial and legal issues…
---

Before we publish the *next* post about the current state of Snowdrift.coop, we're going to give some perspective on the project overall.

Here's one version of my personal Snowdrift.coop origin story / procrastination joke:

I'm just working to further economic democracy on a global scale…  via Snowdrift.coop… in order to better fund the organizing program I use (Task Coach)… so that I can finally do better planning of my grad school applications… so that I can then learn more about music cognition… in order to better inform the creative music I want to make.[[1]](#fn1)

It's easier to laugh about being a procrastinating artist than to own up to the quixotic reality of this project. ![](/content/images/2017/11/1280px-Alcazar_San_Juan_Quijote_JMM.jpg)Image from [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Alcazar_San_Juan_Quijote_JMM.jpg) by user Josemanuel CC BY-SA 2.5

Sure, we have an idealistic [mission](https://wiki.snowdrift.coop/about/mission). But we're also "just" a supporting platform for all the other projects that produce the real value. Downplaying our significance and challenges makes the goal feel more realistic compared to actually facing the level of ambition inherent to Snowdrift.coop.

Who am I to take on this project anyway? I'm not a world-class political leader. I have no real work experience in non-profits or cooperatives (aside from modest volunteering). My only business experiences include assistant management of a small musical instrument store, my years as a self-employed music teacher, and time in a semi-pro band. My web development experience amounts to using some GUI tools to run a simple blog. Before Snowdrift.coop, I had never even used Twitter, never owned a cell phone, and hadn't done any real-time chatting online in over a decade.[[2]](#fn2)

I didn't set out to make my own new thing as an end in itself. I didn't fancy myself some savior for FLO public goods. I assumed that more qualified people were already working on these problems, and the best I could do is lend a hand. It took a lot of convincing from others before I was willing to participate in something new, and I still made a point of triple-checking obsessively checking that we weren't just needlessly competing with any of the [other fundraising platforms](https://wiki.snowdrift.coop/market-research/other-crowdfunding).

I also worked to recruit all the truly qualified people I could. While we still need more support from more people, recruiting was somewhat successful. Thankfully, I can now say that **this is *not* my project, I'm just one member of a [team](https://wiki.snowdrift.coop/community/team)** (and the team is assisted by many other volunteers and partners, although we certainly could [use more help](https://wiki.snowdrift.coop/community/how-to-help) still).

In the end, we did start this thing. Maybe it's for the best that my friend and co-founder, David, seems pathologically optimistic. I initially thought this project would take five years and seemed totally overwhelming. David said he could get an initial prototype operating in a few months. Maybe we're still here, approaching a first working version five years later, because I went along with the optimism and gave it a chance. But now let's take a moment to realize how insanely ambitious this project really is.

## The quixotic challenges inherent in Snowdrift.coop

- Running a complex social platform with all the technology involved and near-zero startup budget
- Living up to the expectations from software-freedom activists (privacy, a fully-free stack ourselves, etc)
- Handling complex legal and monetary issues, internationally even
- Running democratically as an online cooperative[[3]](#fn3)
- Designing and explaining a truly novel method of fundraising
- Introducing and explaining the relevant economic concepts unfamiliar to most people[[4]](#fn4)
- Managing a diverse team of volunteers with inconsistent availability across time-zones and differing native languages using mostly only online communication
- and lots more…

**We have *all* the challenges of any idealistic FLO project *plus* creating a new type of product (not just a FLO version of existing concepts) *plus* the challenges of building a co-op/movement/community *plus* delving into financial and legal issues**…

And yet, somehow we propose to get through all the challenges ourselves just to be in position to help other projects that often face *fewer* challenges than we do!

Our chance to succeed fully comes through taking one step at a time and ignoring/denying the odds against us. But sometimes we need to remember not to be too discouraged when we find it difficult. We can deny all we like, but this is a crazy ambitious project. It's also a project that is worth the pursuit.

In the next blog post, I'll review how far along we are in this crazy adventure and what's coming next…

---

1. 
For more about my personal story, see my [team profile page](https://wiki.snowdrift.coop/community/people/wolftune) and/or this [talk at Open Source Bridge 2015](https://www.youtube.com/watch?v=UoexwmVNmu0). [↩︎](#fnref1)

2. 
Although I had used America Online a little back in the 1990's, I got sick of the superficial nature of most "instant messaging" and stopped using it entirely by around 2002. After installing GNU/Linux in January, 2012, I eventually came upon a situation where I was pushed to use IRC and experienced real culture shock seeing that people were spending substantial time

chatting casually online. When my friend David (now Snowdrift.coop co-founder) heard from a mutual friend about my getting into GNU/Linux, he sent me a Google chat message which was the first unsolicited random online ping from a friend that I'd received in around 10 years. I thought it was all really weird. Of course, it was then my ranting to him about how weird this is and how promising yet screwed up the overall GNU/Linux situation seemed that evolved into the ideas that became Snowdrift.coop. See the documented [Snowdrift.coop history](https://wiki.snowdrift.coop/about/history)[↩︎](#fnref2)

3. 
And we have no established precedent to follow for building a full online co-op. So, we are blazing the trail ourselves — although we're one of several projects pushing forward here as part of [Platform Cooperativism](https://platform.coop/). [↩︎](#fnref3)

4. 
Consider the [tyranny of concision](https://www.youtube.com/watch?v=uXPvPxOFba0) described by Noam Chomsky (an important and valid concept regardless of whether you agree or disagree with Chomsky's political claims otherwise). [↩︎](#fnref4)
