---
title: Plans for 2015
slug: 2015-plans
date_published: 2015-01-10T06:08:00.000Z
date_updated: 2017-11-11T00:39:14.000Z
excerpt: After our successful fund-drive, we feel optimistic about 2015 but still have so much to do. We have several milestones to work toward as we get ready for full operations.
---

After our [successful fund-drive](https://snowdrift.tilt.com), we feel optimistic about 2015 but still have so much to do. We have several milestones to work toward as we get ready for full operations. To help us get there, please see our updated page on [how-to-help with Snowdrift.coop](https://wiki.snowdrift.coop/community/how-to-help) — and if anything there isn't clear, help us improve that page! Every page has associated discussion boards…

---

In particular, we need to finalize our [legal standing](https://wiki.snowdrift.coop/legal), and while our newly-raised funds will help greatly, we need to take care in how we use our still-tight budget. As we work with a lawyer, we will also continue researching on our own and recruiting volunteer help from those with cooperative and non-profit experience.

In other major news, co-founder and lead developer David Thomas has accepted a day job. To keep up our momentum, David will both assist in funding a new lead developer and will continue to help code in his spare time. See our [job openings](https://wiki.snowdrift.coop/archives/general-management/jobs) page for more about the position.

As we push ahead, we'll continue promoting and building the community. We are making plans to participate in all sorts of conferences. We may even participate in one conference per month or more such as [SCALE](http://www.socallinuxexpo.org/scale/13x) in February, [LibrePlanet](https://libreplanet.org/2015) in March, [Linux Fest Northwest](http://linuxfestnorthwest.org/2015) in April, BayHac in May (assuming that goes ahead like last year), [Open Source Bridge](http://opensourcebridge.org) in June, and many others. Of course, while the main team can't make it to every event, we welcome any community supporters to help us spread the word and promote elsewhere.

While we don't have certainty about how 2015 will go, it will surely be a significant year. Thanks to everyone who has taken interest in what we're doing!
