---
title: Announcing our crowdfunding campaign
slug: fund-drive-launch-2014-11-27
date_published: 2014-11-27T22:04:00.000Z
date_updated: 2017-11-17T05:20:29.000Z
excerpt: Our launch campaign at Snowdrift.Tilt.com is now live! The open.tilt.com hosting we chose requires the main video to be at YouTube (which involves tracking and proprietary formats and other issues). So, we've also uploaded the video to Archive.org…
---

**Our launch campaign at Snowdrift.Tilt.com is now live!** The open.tilt.com hosting we chose requires the main video to be at YouTube (which involves tracking and proprietary formats and other issues). So, we've also uploaded the video to Archive.org (which respects user privacy and offers both streaming and download in the free/libre/open .ogg format):

## Why Tilt Open?

At Snowdrift.coop, we've worked to maintain the very highest standards for our technology and policies. When it came to running a one-time fund-drive, we wanted to continue in that direction. Yet **in all our [reviews of crowdfunding sites](https://wiki.snowdrift.coop/market-research/other-crowdfunding), the only no-compromise option we found was to self-host**. So, we *considered* either building our own tools or setting up a hosted version of existing software…

**In the end, we determined that the work in even setting up our own hosting was too far outside our main mission**. Long-term, we want to provide a more sustainable alternative to these expensive one-time campaigns. Taking time and energy to set up the perfect one-time campaign does not serve that long-term goal.

---

So, we accepted some compromise but still wanted to choose the best of the practical options among services that would host a campaign for us.  It came down to choosing between Open.Tilt.com and Goteo.org. Both use FLO terms for their own software. Tilt is simpler, plainer, and cheaper. Goteo is more international (based in Spain) and more dedicated to FLO issues in terms of the projects they support. Unfortunately, *both* sites use several proprietary services and trackers including Google Analytics, Facebook, and YouTube (among others). Of course, we opted *not* to enable the extra trackers and analytics that Tilt offered.  Among all the sites we've reviewed, [FreedomSponsors.org](http://freedomsponsors.org) is the only significant one that makes more of an effort to do things like avoid third-party tracking and work without proprietary Javascript.  They still aren't perfect as they encourage log-in through corporate tracking sites, but at least they don't *require* that. Anyway, FreedomSponsors.org does not do big fund-drives. They support bounties to fix and improve software, so they do not offer what we needed now.

For less technical readers, all this means that, like most sites on the internet, most crowdfunding sites are intertwined with (or in some cases *are*) corporate proprietary and privacy-invading systems. Such is the state of the internet today; and *that* is one of many reasons we need Snowdrift.coop to succeed. **Essentially, the path of least resistance today does not leads one to free/libre/open options.**

Tilt Open is at least better than most and happens also to be the best option to adapt if one wants to go all the way and self-host a crowdfunding campaign.

## Clearing the path for the future

**Snowdrift.coop will not only model a higher standard of respect for the community, it will help push others in the same direction**. We aim to give the general community more economic power to focus our collective funding on the most honorable projects.

In fact, you should *not* have to learn about all these things. Most internet users should *not* have to think about all these technical ramifications of different sites and services. Neither should you have to worry about sharing music and art with your friends, online or otherwise. As much as we want more people to care about these issues, **you should not have to be a technical or legal expert to have privacy, dignity, and freedoms in your use of the internet and our digital commons.**

With our curated platform where everything meets a minimum standard and reports transparently on some other items of concern, general users will not carry the burden of evaluating all the fine details.  **With your help, our campaign will succeed, and we will clear a *new* path of least-resistance which leads us in a direction we actually want to go.**
