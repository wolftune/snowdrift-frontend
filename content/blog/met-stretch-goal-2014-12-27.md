---
title: Fund-drive stretch goal met, a few days left
slug: met-stretch-goal-2014-12-27
date_published: 2014-12-28T07:21:00.000Z
date_updated: 2017-11-17T05:23:27.000Z
excerpt: With four days remaining in our launch fund-drive, a generous donation from Linux Fund put us over the top for our stretch goal of $8,000 at the Tilt Open campaign. Adding the outside donations via Paypal, Dwolla, and Bitcoin, our total raised is over $14,000 from over 200 supporters.
---

With four days remaining in our launch fund-drive, a generous donation from [Linux Fund](http://linuxfund.org) put us over the top for our stretch goal of $8,000 at the Tilt Open campaign. Adding the outside donations via Paypal, Dwolla, and Bitcoin, our total raised is over $14,000 from over 200 supporters.

Our supporters have largely come from the software community, although one of the key founding patrons at the $500 level is hardware project [Aleph Objects](https://www.alephobjects.com) (makers of the fully-FLO Lulzbot 3D printer). On the less technical side, I was a guest on some [Free Culture podcasts](https://wiki.snowdrift.coop/about/presentations#aaron-as-guest-on-podcasts) and, like me, several of our supporters come from the music and art side of the FLO world. The 630+ users now registered on Snowdrift.coop also include people involved in science and education.

We'll have more details about the campaign in a week or two. Meanwhile, we still welcome further donations and promotion in these last few days of the fund-drive.

We'll have some other big announcements soon about technical progress, but we do have some notable updates to share already…

---

## Recent technical progress

We've seen about 30 commits during the current month ranging from several minor items to a couple important features including the ability to delete or archive notification messages and better info about the impact of each pledge when placing it.

Nikita already has work in progress on an initial version of notifications for users to get when they choose to "watch" select projects. That will go live soon along with full e-mail integration (currently in testing). David has been busy fixing bugs and steadily knocking out priority tickets.

We care about making things as easy for volunteers as possible, so we've already made our build process quite smooth and well-documented, but we'll soon have nearly one-command deployment thanks to Miëtek Bak working to set up Snowdrift.coop with his [Halcyon](https://halcyon.sh) project. Thanks also to all the other volunteers now hacking on the site. Every bit adds up and helps us get closer to launching.

The next big milestones also include generated graphs to help patrons better understand their pledges and the full project sign-up process along with getting more initial projects listed.

My own work is all over the place. I recently made many cosmetic improvements and minor fixes to the site. Mostly, I've been busy helping volunteers and other newcomers make sense of things, researching legal issues, doing interviews and other promotion, and working on new videos.

Also, a lot of people have suggested that they would like to understand more about the Snowdrift.coop team, our motivations and backgrounds. For now, at the listing of the [snowdrift team](https://wiki.snowdrift.coop/community/team), you can click the links to see each full profile. Some of us have written more than others. My profile contains a full background story leading up to founding Snowdrift.coop. In some future blog posts, we will feature more details about each of us on the team and our thoughts looking forward.

Thanks everyone for your support! Stay with us into 2015, and check in regularly. Soon, we'll make it even easier to stay up-to-date with our progress so you won't need to manually check the site.
