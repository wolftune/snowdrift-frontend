# Elm-pages 2.0

Upgrading the site to the new elm-pages 2.0 platform, simplifying a lot of boilerplate. Soon, Elm-pages 3.0


# Styles
- Going for a [utility classes approach](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/) and [atomic css](https://johnpolacek.github.io/the-case-for-atomic-css/). Decided [Tailwind](https://tailwindcss.com/) was the nicest system to use. We also skip the dependencies: no need to compile tailwind with Webpack as [this Elm package](https://package.elm-lang.org/packages/matheus23/elm-default-tailwind-modules/latest/) uses the Elm compiler to handle that instead. This works in conjunction with elm-css for type-safe styling.

# Routes
Any part of the module that doesn't end with an underscore (_) is a static segment.

For example, in src/Page/About.elm, about is a static segment, so the URL /about will route to this module.

Static segments are CapitalCamelCase in Elm module names, and are kebab-case in the URL. So src/Page/OurTeam.elm will handle the URL /our-team.

Segments can be nested. So src/Page/Jobs/Marketing will handle the URL /jobs/marketing.

There is one special static segment name: Index. You'll only need this for your root route. So src/Page/Index.elm will handle the URL /.

Segments ending with an underscore (\_) are dynamic segments. You can mix static and dynamic segments. For example, src/Page/Blog/Slug\_.elm will handle URLs like /blog/my-post.
