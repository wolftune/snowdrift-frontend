module View exposing (View(..), map, placeholder)

{-| View.elm

Defines the types for your application's View msg type. Must expose

    A type called View msg (must have exactly one type variable)
    map : (msg1 -> msg2) -> Document msg1 -> Document msg2
    placeholder : String -> View msg - used in when you scaffold a new Page module with elm-pages add MyRoute

The View msg type is what individual Page/ modules must return in their view functions. So if you want to use mdgriffith/elm-ui in your Page's view functions, you would update your module like this:

-}

import Html
import Html.Styled


{-| For this project we may want to support different ways of generating Html, so we'll just prepare for any of them.
-}
type View msg
    = PlainHtml (PlainHtmlView msg)
    | ElmCSSStyledHtml (StyledHtmlView msg)


{-| For pages that are just plain html
-}
type alias PlainHtmlView msg =
    { title : String
    , body : List (Html.Html msg)
    }


{-| For pages that use elm-css, such as for tailwind
-}
type alias StyledHtmlView msg =
    { title : String
    , body : List (Html.Styled.Html msg)
    }


{-| We need to provide this to the internal engine so it knows how to map Html msgs regardless of how we set up the view.
-}
map : (msg1 -> msg2) -> View msg1 -> View msg2
map fn doc =
    case doc of
        PlainHtml plainDoc ->
            PlainHtml
                { title = plainDoc.title
                , body = List.map (Html.map fn) plainDoc.body
                }

        ElmCSSStyledHtml styledDoc ->
            ElmCSSStyledHtml
                { title = styledDoc.title
                , body = List.map (Html.Styled.map fn) styledDoc.body
                }


placeholder : String -> View msg
placeholder moduleName =
    PlainHtml
        { title = "Under Construction (" ++ moduleName ++ ")"
        , body = [ Html.text moduleName ]
        }
