module Site exposing (config)

{-| Defines global head tags and your manifest config.

Must expose

    config : SiteConfig StaticData

-}

import DataSource
import Head
import Pages.Manifest as Manifest
import Route
import SiteConfig exposing (SiteConfig)


type alias Data =
    ()


config : SiteConfig Data
config =
    { data = data
    , canonicalUrl = "https://new.snowdrift.coop"
    , manifest = manifest
    , head = head
    }


data : DataSource.DataSource Data
data =
    DataSource.succeed ()


head : Data -> List Head.Tag
head static =
    [ Head.sitemapLink "/sitemap.xml"
    ]


manifest : Data -> Manifest.Config
manifest static =
    Manifest.init
        { name = "Snowdrift"
        , description = "Crowdmatching for Public Goods"
        , startUrl = Route.Index |> Route.toPath
        , icons = []
        }
