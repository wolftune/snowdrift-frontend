module Page.HowItWorks exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Html.Styled as StyledHtml exposing (..)
import Html.Styled.Attributes as StyledHtmlAttributes exposing (..)
import Page exposing (Page, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Shared
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    {}


page : Page RouteParams Data
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildNoState { view = view }


data : DataSource Data
data =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Snowdrift"
        , image =
            { url = Pages.Url.external "images/logos/snowdrift/dark-blue-raster.png"
            , alt = "elm-pages logo"
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = "TODO"
        , locale = Nothing
        , title = "How Snowdrift.coop works" -- metadata.title -- TODO
        }
        |> Seo.website


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    View.ElmCSSStyledHtml
        { title = "How Snowdrift.coop works"
        , body =
            [ main_
                [ css
                    [ w_full
                    , py_16 -- move main content down from touching header
                    , grid_cols_12
                    , object_center
                    ]
                ]
                [ h1 [ css [ text_center, font_bold, text_4xl ] ] [ text "How It Works" ]
                , explainerSection
                , Shared.signupButtonLearnMore
                ]
            ]
        }


explainerSection =
    section [ css [ m_auto ] ]
        [ img
            [ css [ m_auto, max_w_3xl, py_32 ]
            , src "images/explainers/project_crowdmatching.png"
            ]
            []
        , div [ css [ px_8, rounded_3xl, py_8, bg_gray_100, m_auto, max_w_xl, text_2xl, font_light ] ]
            [ p [] [ text "Every patron matches every other." ]
            , p [] [ text "Projects receive their donations monthly. " ]
            ]
        ]
