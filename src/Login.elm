module Login exposing (..)

import Graphql.Document as Document
import Graphql.Http
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Http
import Json.Decode
import Json.Encode
import Payload.Mutation as Mutation
import Payload.Object exposing (UserFields_apiKey)
import Payload.Object.User
import Payload.Object.UsersLoginResult
import Payload.Object.UsersMe
import Payload.Query as Query
import Remote.Data as RemoteData exposing (GraphqlHttpData)
import Remote.Errors as RemoteErrors
import Remote.Recyclable as Recyclable exposing (GraphqlHttpRecyclable)
import Remote.Response as Response exposing (GraphqlHttpResponse)


apiLocation =
    "http://localhost:3000/api/graphql/"


type alias LoginResponse =
    Result LastLoginFailureDetails LoggedInDetails


type LoginStatus
    = LoggedOut (Maybe LastLoginFailureDetails)
    | LoggedIn LoggedInDetails


type alias LoggedInDetails =
    { token : Maybe String
    , user : Maybe User
    , expires : Maybe Int
    }


type alias LastLoginFailureDetails =
    { problem : String
    }


type alias User =
    { name : Maybe String
    }


getLoggedInUser : LoggedInDetails -> Result String { token : String, user : User, expires : Int }
getLoggedInUser withMaybes =
    case ( withMaybes.token, withMaybes.user, withMaybes.expires ) of
        ( Just foundToken, Just foundUser, Just foundExpires ) ->
            Ok <|
                { token = foundToken
                , user = foundUser
                , expires = foundExpires
                }

        ( Just foundToken, Nothing, Just foundExpires ) ->
            Err <| "Got token, expiration... Missing user!"

        ( Nothing, Just _, Just foundExpires ) ->
            Err <| "Got user, expiration... Missing token!"

        ( Just _, Just _, Nothing ) ->
            Err <| "Got token, user... Missing expiration!"

        ( Just _, Nothing, Nothing ) ->
            Err <| "Got token, but missing both user and expiration!"

        ( Nothing, Nothing, Just _ ) ->
            Err <| "Got expiration, but missing both user and token!"

        ( Nothing, Just _, Nothing ) ->
            Err <| "Got user, but missing both token and expiration!"

        ( Nothing, Nothing, Nothing ) ->
            Err <| "User, expiration, and token were all missing..."


toUser : SelectionSet User Payload.Object.User
toUser =
    SelectionSet.map User
        Payload.Object.User.name


toLoggedInDetails : SelectionSet LoggedInDetails Payload.Object.UsersLoginResult
toLoggedInDetails =
    SelectionSet.map3 LoggedInDetails
        Payload.Object.UsersLoginResult.token
        (Payload.Object.UsersLoginResult.user toUser)
        Payload.Object.UsersLoginResult.exp


meUserToLoggedInDetails : SelectionSet LoggedInDetails Payload.Object.UsersMe
meUserToLoggedInDetails =
    SelectionSet.map3 LoggedInDetails
        Payload.Object.UsersMe.token
        (Payload.Object.UsersMe.user toUser)
        Payload.Object.UsersMe.exp


checkLogin : SelectionSet (Maybe LoggedInDetails) RootQuery
checkLogin =
    Query.meUser meUserToLoggedInDetails


makeGraphCheckLoginRequest : (GraphqlHttpResponse LastLoginFailureDetails LoggedInDetails -> msg) -> Cmd msg
makeGraphCheckLoginRequest responseWrapper =
    let
        tagger : GraphqlHttpResponse LastLoginFailureDetails LoggedInDetails -> msg
        tagger =
            responseWrapper

        responseToMsg : Result (Graphql.Http.Error (Maybe LoggedInDetails)) (Maybe LoggedInDetails) -> msg
        responseToMsg result =
            case Graphql.Http.discardParsedErrorData result of
                -- TODO figure this out
                Ok loggedInResponse ->
                    case loggedInResponse of
                        Just loggedInDetails ->
                            -- may return a blank loggedInDetails record if not logged in
                            Response.graphqlHttpToMsg tagger (Ok <| Ok loggedInDetails)

                        Nothing ->
                            Response.graphqlHttpToMsg tagger (Ok <| Err <| LastLoginFailureDetails "Not already logged in.")

                Err (Graphql.Http.GraphqlError _ [ graphQlErrorSingleton ]) ->
                    Response.graphqlHttpToMsg tagger
                        (Ok (Err (LastLoginFailureDetails graphQlErrorSingleton.message)))

                Err (Graphql.Http.HttpError Graphql.Http.NetworkError) ->
                    Response.graphqlHttpToMsg tagger (Ok <| Err <| LastLoginFailureDetails ("Network error. Is the server running, and is the API accessible at " ++ apiLocation ++ " ?"))

                Err (Graphql.Http.HttpError Graphql.Http.Timeout) ->
                    Response.graphqlHttpToMsg tagger (Ok <| Err <| LastLoginFailureDetails "That's weird, the server took too long to respond. Maybe it's overburdened, or maybe your connection is too slow?")

                Err otherProblem ->
                    Response.graphqlHttpToMsg tagger (Ok <| Err <| { problem = "Unusual Error, need to debug. "
                    --  ++ Debug.toString otherProblem 
                     })
    in
    checkLogin
        |> Graphql.Http.queryRequest apiLocation
        |> Graphql.Http.withCredentials
        |> Graphql.Http.send responseToMsg


attemptLogin : { user : String, pass : String } -> SelectionSet LoginResponse RootMutation
attemptLogin { user, pass } =
    let
        passCredentials : Mutation.LoginUserOptionalArguments -> Mutation.LoginUserOptionalArguments
        passCredentials defaultArgs =
            -- TODO must these really be "optional" args?
            { defaultArgs
                | email = Present user
                , password = Present pass
            }

        maybeToResult gotLoggedInDetailsMaybe =
            case gotLoggedInDetailsMaybe of
                Just loggedInDetails ->
                    Ok loggedInDetails

                Nothing ->
                    Err { problem = "did not get logged in details back" }
    in
    Mutation.loginUser
        passCredentials
        toLoggedInDetails
        |> SelectionSet.map maybeToResult


makeGraphLoginRequest : { user : String, pass : String } -> (GraphqlHttpResponse LastLoginFailureDetails LoggedInDetails -> msg) -> Cmd msg
makeGraphLoginRequest credentials responseWrapper =
    let
        responseToMsg : Result (Graphql.Http.Error LoginResponse) LoginResponse -> msg
        responseToMsg result =
            case Graphql.Http.discardParsedErrorData result of
                -- TODO figure this out
                Ok loggedInResponse ->
                    case loggedInResponse of
                        Ok loggedInDetails ->
                            Response.graphqlHttpToMsg tagger (Ok <| Ok loggedInDetails)

                        Err lastLoginFailureDetails ->
                            Response.graphqlHttpToMsg tagger (Ok <| Err <| lastLoginFailureDetails)

                Err (Graphql.Http.GraphqlError _ [ graphQlErrorSingleton ]) ->
                    Response.graphqlHttpToMsg tagger
                        (Ok (Err (LastLoginFailureDetails graphQlErrorSingleton.message)))

                Err (Graphql.Http.HttpError Graphql.Http.NetworkError) ->
                    Response.graphqlHttpToMsg tagger (Ok <| Err <| LastLoginFailureDetails ("Network error. Is the server running, and is the API accessible at " ++ apiLocation ++ " ?"))

                Err (Graphql.Http.HttpError Graphql.Http.Timeout) ->
                    Response.graphqlHttpToMsg tagger (Ok <| Err <| LastLoginFailureDetails "That's weird, the server took too long to respond. Maybe it's overburdened, or maybe your connection is too slow?")

                Err otherProblem ->
                    Response.graphqlHttpToMsg tagger (Ok <| Err <| { problem = "Unusual Error, need to debug. "
                    --  ++ Debug.toString otherProblem 
                     })

        tagger : GraphqlHttpResponse LastLoginFailureDetails LoggedInDetails -> msg
        tagger =
            responseWrapper
    in
    attemptLogin credentials
        |> Graphql.Http.mutationRequest apiLocation
        |> Graphql.Http.withCredentials
        |> Graphql.Http.send responseToMsg


logoutRequest : (String -> msg) -> Cmd msg
logoutRequest responseWrapper =
    let
        loggedOutSuccessfully result =
            case result of
                Ok (Just output) ->
                    responseWrapper (output)

                Ok Nothing ->
                    responseWrapper "Logged out !"

                Err problem ->
                    responseWrapper ("Failed to log out. " ++
                    --  "Problem was " ++ Debug.toString problem
                    "Needs debug."
                     )
    in
    Mutation.logoutUser
        |> Graphql.Http.mutationRequest apiLocation
        |> Graphql.Http.withCredentials
        |> Graphql.Http.send loggedOutSuccessfully



-- REST API


type alias UserDetails =
    { token : String }


loginRequest : { user : String, pass : String } -> (Maybe String -> msg) -> Cmd msg
loginRequest { user, pass } returner =
    let
        body =
            Json.Encode.object
                [ ( "email", Json.Encode.string user )
                , ( "password", Json.Encode.string pass )
                ]

        responseDecoder =
            Json.Decode.map UserDetails
                (Json.Decode.field "token" Json.Decode.string)

        responseWrapper result =
            case result of
                Ok { token } ->
                    returner (Just token)

                Err httpErr ->
                    returner <| Nothing
    in
    Http.request
        { method = "POST"
        , headers = [ Http.header "Content-Type" "application/json" ]
        , url = "http://localhost:3000/api/users/login"
        , body = Http.jsonBody body
        , expect = Http.expectJson responseWrapper responseDecoder
        , timeout = Nothing
        , tracker = Nothing
        }
