/** @typedef {{load: (Promise<unknown>); flags: (unknown)}} ElmPagesInit */

/** This is the entrypoint for your JavaScript. Export an Object with a functions load and flags. Right now, this is the only place that user JavaScript code can be loaded. You can use import statements to load other JS files here.

load is an async function that will be called with a Promise that you can await to register ports on your Elm application (or just wait until the Elm application is loaded).

flags will be passed in to your Flags in your Shared.elm module.
*/


/** @type ElmPagesInit */
export default {
  load: async function (elmLoaded) {
    const app = await elmLoaded;
    console.log("App loaded", app);
  },
  flags: function () {
    return "You can decode this in Shared.elm using Json.Decode.string!";
  },
};
